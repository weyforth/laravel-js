<?php
/**
 * JsVar Container.
 *
 * Allows namespaced container, used by the JsVar class to provide
 * a convenience interface for adding and retrieving values.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\JS;

class JsVarContainer
{

    /**
     * Name of container.
     *
     * @var string
     */
    public $name;

    /**
     * Variables store.
     *
     * @var array
     */
    public $vars = array();


    /**
     * Constructor.
     *
     * @param string $name Name of container.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }


    /**
     * Constructor.
     *
     * @param string $name  Key for added variable.
     * @param mixed  $value Value to add.
     *
     * @return Weyforth\JS\JsVarContainer For chainability
     */
    public function add($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $key => $value) {
                if (array_key_exists($key, $this->vars) && is_array($value) && is_array($this->vars[$key])) {
                    $this->vars[$key] = array_merge_recursive($this->vars[$key], $value);
                } else {
                    $this->vars[$key] = $value;
                }
            }
        } else {
            if (array_key_exists($name, $this->vars) && is_array($value) && is_array($this->vars[$name])) {
                $this->vars[$name] = array_merge_recursive($this->vars[$name], $value);
            } else {
                $this->vars[$name] = $value;
            }
        }

        return $this;
    }


    /**
     * Retrieve the stored variables.
     *
     * For inclusion in a script file or tag.
     * Either separate into var declarations or encode.
     *
     * @param boolean $object Whether to return a JSON encoded object or var declaration.
     *
     * @return string Encoded data
     */
    public function get($object = false)
    {
        $jsonOptions = (JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);

        if ($object) {
            return json_encode(
                $this->vars,
                $jsonOptions
            );
        }

        $output = array();

        foreach ($this->vars as $key => $value) {
            $encoded  = json_encode(
                $value,
                $jsonOptions
            );
            $output[] = 'var '.$key.' = '.$encoded.';';
        }

        return implode("\n", $output);
    }


}
