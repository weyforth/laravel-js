<?php
/**
 * JsVar Class.
 *
 * Utility class to make it easy to store key-value pairs to
 * include in template script tags. Outputs json encoded values
 * or var declarations.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\JS;

class JsVar
{

    /**
     * Store of all created containers.
     *
     * @var array
     */
    public static $containers = array();


    /**
     * Retrieve the container with the supplied name.
     *
     * Will create a container with the supplied name
     * if it doesn't already exist.
     *
     * @param string $container Name of container.
     *
     * @return Weyforth\JS\JsVarContainer
     */
    public static function container($container = 'default')
    {
        if (!isset(static::$containers[$container])) {
            static::$containers[$container] = new JsVarContainer($container);
        }

        return static::$containers[$container];
    }


    /**
     * Magic method.
     *
     * Passes method calls through to the default container.
     *
     * @param string $method     Method name.
     * @param mixed  $parameters Parameters of called method.
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return call_user_func_array(
            array(
                static::container(),
                $method
            ),
            $parameters
        );
    }


}
